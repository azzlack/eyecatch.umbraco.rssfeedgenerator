﻿namespace EyeCatch.Umbraco.FeedGenerator
{
    using System;

    /// <summary>
    /// A feed item.
    /// </summary>
    public class FeedItem
    {
        /// <summary>The title.</summary>
        public string Title { get; set; }

        /// <summary>The url.</summary>
        public string Url { get; set; }

        /// <summary>The publication date in UTC.</summary>
        public DateTime PublishDate { get; set; }

        /// <summary>The last update date in UTC.</summary>
        public DateTime UpdateDate { get; set; }

        /// <summary>The author.</summary>
        public string Author { get; set; }

        /// <summary>The tags.</summary>
        public string[] Tags { get; set; }

        /// <summary>The summary.</summary>
        public string Summary { get; set; }

        /// <summary>The full content.</summary>
        public string Content { get; set; }
    }
}
﻿namespace EyeCatch.Umbraco.FeedGenerator
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;

    using global::Umbraco.Core.Models;
    using global::Umbraco.Web;
    using global::Umbraco.Web.Models;

    public class FeedModel
    {
        /// <summary>The source content.</summary>
        private IPublishedContent source;

        /// <summary>The site base url.</summary>
        private string baseUrl;
        
        /// <summary>The feed items.</summary>
        private List<FeedItem> items; 

        public FeedModel(PartialViewMacroModel model)
        {
            var source = model.MacroParameters["Source"] ?? string.Empty;
            var baseUrl = model.MacroParameters["BaseUrl"] ?? string.Empty;
            var documentTypeAlias = model.MacroParameters["DocumentTypeAlias"] ?? string.Empty;
            var keywordProperty = model.MacroParameters["KeywordProperty"] ?? string.Empty;
            var summaryTextProperty = model.MacroParameters["SummaryTextProperty"] ?? string.Empty;
            var fullTextProperty = model.MacroParameters["FullTextProperty"] ?? string.Empty;
            int maxLevel = 0;
            if (model.MacroParameters["MaxLevel"] != null)
            {
                int.TryParse(model.MacroParameters["MaxLevel"].ToString(), out maxLevel);
            }

            this.Initialize(
                model.Content,
                source.ToString(),
                baseUrl.ToString(),
                documentTypeAlias.ToString().Trim('[', ']').Split(',').Where(x => !string.IsNullOrEmpty(x)).ToArray(),
                keywordProperty.ToString().Trim('[', ']'),
                summaryTextProperty.ToString().Trim('[', ']'),
                fullTextProperty.ToString().Trim('[', ']'),
                maxLevel);
        }

        /// <summary>Gets the title.</summary>
        /// <value>The title.</value>
        public string Title
        {
            get
            {
                return this.source.GetPropertyValue<string>(
                    "rssTitle",
                    new Uri(this.source.UrlWithDomain()).Host + " RSS Feed");
            }
        }

        /// <summary>Gets URL of the site.</summary>
        /// <value>The site URL.</value>
        public string SiteUrl
        {
            get
            {
                return this.baseUrl;
            }
        }

        /// <summary>Gets URL of the feed.</summary>
        /// <value>The feed URL.</value>
        public string SelfUrl
        {
            get
            {
                try
                {
                    return HttpContext.Current.Request.Url.AbsoluteUri;
                }
                catch (Exception)
                {
                    return null;
                }
            }
        }

        /// <summary>Gets the publication date.</summary>
        /// <value>The publication date.</value>
        public DateTime Updated
        {
            get
            {
                if (!this.Items.Any())
                {
                    return this.source.UpdateDate.ToUniversalTime();
                }

                return this.Items.Max(x => x.UpdateDate);
            }
        }

        /// <summary>Gets the feed description.</summary>
        /// <value>The feed description.</value>
        public string Description
        {
            get
            {
                return this.source.GetPropertyValue<string>("rssDescription", true, string.Empty);
            }
        }
        
        /// <summary>Gets the feed language.</summary>
        /// <value>The feed language.</value>
        public string Language
        {
            get
            {
                var l = this.source.GetPropertyValue<string>("rssLanguage", true, string.Empty);

                if (string.IsNullOrEmpty(l))
                {
                    return this.source.GetCulture().Name;
                }

                return l;
            }
        }

        /// <summary>Gets the feed items.</summary>
        /// <value>The feed items.</value>
        public List<FeedItem> Items
        {
            get
            {
                return this.items ?? (this.items = new List<FeedItem>());
            }
        }

        /// <summary>Gets the Umbraco helper for the current request.</summary>
        private UmbracoHelper Umbraco
        {
            get
            {
                return new UmbracoHelper(UmbracoContext.Current);
            }
        }

        /// <summary>Initializes this instance.</summary>
        /// <param name="content">The current content.</param>
        /// <param name="source">The source content.</param>
        /// <param name="baseUrl">The base url.</param>
        /// <param name="documentTypeAliases">The document type aliases.</param>
        /// <param name="keywordProperty">The keyword property.</param>
        /// <param name="summaryTextProperty">The summary text property.</param>
        /// <param name="fullTextProperty">The full text property.</param>
        /// <param name="maxDepth">The maximum number of levels below the source to traverse.</param>
        internal void Initialize(IPublishedContent content, string source, string baseUrl, string[] documentTypeAliases, string keywordProperty, string summaryTextProperty, string fullTextProperty, int maxDepth)
        {
            // Populate Source
            if (source != null && !string.IsNullOrEmpty(source))
            {
                // Check if this is a media item first
                var media = this.Umbraco.TypedMedia(source);

                this.source = media ?? this.Umbraco.TypedContent(source);
            }
            else
            {
                this.source = content;
            }

            // Populate Base Url
            if (baseUrl != null && !string.IsNullOrEmpty(baseUrl))
            {
                this.baseUrl = baseUrl;
            }
            else
            {
                var u = new Uri(content.UrlWithDomain());
                this.baseUrl = string.Format("{0}://{1}", u.Scheme, u.Host);
            }

            // Populate Items
            var descendants = this.source.Descendants();

            if (maxDepth > 0)
            {
                descendants = descendants.Where(x => x.Level <= (this.source.Level + maxDepth));
            }

            if (documentTypeAliases.Any())
            {
                descendants = descendants.Where(x => documentTypeAliases.Contains(x.DocumentTypeAlias));
            }

            foreach (var descendant in descendants)
            {
                var item = new FeedItem()
                               {
                                   Title = descendant.Name,
                                   PublishDate = descendant.CreateDate.ToUniversalTime(),
                                   UpdateDate = descendant.UpdateDate.ToUniversalTime(),
                                   Url = descendant.UrlWithDomain(),
                                   Author = descendant.CreatorName
                               };

                if (!string.IsNullOrEmpty(keywordProperty) && descendant.HasValue(keywordProperty))
                {
                    item.Tags = descendant.GetPropertyValue<string>(keywordProperty).Split(',');
                }
                else
                {
                    item.Tags = new string[0];
                }

                if (!string.IsNullOrEmpty(summaryTextProperty) && descendant.HasValue(summaryTextProperty))
                {
                    var s = descendant.GetPropertyValue<string>(summaryTextProperty);
                    var processed = new HtmlDocumentProcessor().EncodeUris(descendant, s, baseUrl);

                    item.Summary = processed.DocumentNode.InnerHtml;
                }

                if (!string.IsNullOrEmpty(fullTextProperty) && descendant.HasValue(fullTextProperty))
                {
                    var c = descendant.GetPropertyValue<string>(fullTextProperty);
                    var processed = new HtmlDocumentProcessor().EncodeUris(descendant, c, baseUrl);

                    item.Content = processed.DocumentNode.InnerHtml;
                }

                this.Items.Add(item);
            }
        }
    }
}
﻿namespace EyeCatch.Umbraco.FeedGenerator
{
    using System.Web;

    using global::Umbraco.Core.Models;
    using global::Umbraco.Web;

    using HtmlAgilityPack;

    public class HtmlDocumentProcessor
    {
        /// <summary>Gets the Umbraco helper for the current request.</summary>
        private UmbracoHelper Umbraco
        {
            get
            {
                return new UmbracoHelper(UmbracoContext.Current);
            }
        }

        /// <summary>
        /// Encodes the uris for the specified document.
        /// </summary>
        /// <param name="content">The content.</param>
        /// <param name="html">The HTML.</param>
        /// <param name="baseUrl">The base url.</param>
        /// <returns>The html with encoded uris.</returns>
        public HtmlDocument EncodeUris(IPublishedContent content, string html, string baseUrl)
        {
            if (!string.IsNullOrEmpty(html))
            {
                // Load html
                var doc = new HtmlDocument();
                doc.LoadHtml(html);

                doc = this.EncodeLinkUris(doc, baseUrl, content);

                doc = this.EncodeImageUris(doc, baseUrl);

                return doc;
            }

            return null;
        }

        /// <summary>
        /// Encodes the link uris in the specified document.
        /// </summary>
        /// <param name="document">The document.</param>
        /// <param name="baseUrl">The base url.</param>
        /// <param name="content">The content.</param>
        /// <returns>The document with encoded link uris.</returns>
        private HtmlDocument EncodeLinkUris(HtmlDocument document, string baseUrl, IPublishedContent content)
        {
            // Change locallinks
            var links = document.DocumentNode.SelectNodes("//a[@href]");

            if (links != null)
            {
                foreach (var link in links)
                {
                    var href = link.Attributes["href"];

                    // Replace localLink
                    if (href != null && href.Value.StartsWith("/{localLink:"))
                    {
                        int id;
                        var nodeid = href.Value.Substring(12, href.Value.Length - 13);

                        if (int.TryParse(nodeid, out id))
                        {
                            href.Value = this.Umbraco.TypedMedia(id).UrlWithDomain();
                        }

                        // Set absolute uri references
                        if (href.Value.StartsWith("#"))
                        {
                            href.Value = baseUrl + content.Url + href.Value;
                        }
                    }
                }
            }

            return document;
        }

        /// <summary>
        /// Encodes the image uris in the specified document.
        /// </summary>
        /// <param name="document">The document.</param>
        /// <param name="baseUrl">The base url.</param>
        /// <returns>The document with encoded image uris.</returns>
        private HtmlDocument EncodeImageUris(HtmlDocument document, string baseUrl)
        {
            // Fix image paths
            var images = document.DocumentNode.SelectNodes("//img[@src]");

            if (images != null)
            {
                foreach (var image in images)
                {
                    var src = image.Attributes["src"];

                    if (src != null)
                    {
                        // Encode uri
                        src.Value = HttpUtility.UrlPathEncode(src.Value);

                        // Set absolute uri
                        if (src.Value.StartsWith("/"))
                        {
                            src.Value = baseUrl + src.Value;
                        }
                    }
                }
            }

            return document;
        }
    }
}